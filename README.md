# publicconfigs

My public configurations for my setup, and small scripts that help out.

## rc

Configs, rc files, aliases, and X setup.

## scripts

Scripts that help in day to day operations.
