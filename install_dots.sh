#!/bin/sh
[ -d ~/.config/\{bash,zsh,vim,sckless\} ] || mkdir -p ~/.config/{bash,zsh,vim,sckless}
rsync -a --delete ./rc/X11 ~/.config
rsync -a --delete ./rc/bash/.bashrc ~/.config/bash 
rsync -a --delete ./rc/zsh/.zshrc ~/.config/zsh 
rsync -a --delete ./rc/vim/.vimrc ~/.config/vim 
rsync -a --delete ./rc/env ~/.config
rsync -a --delete ./rc/aliasrc ~/.config
rsync -a --delete ./rc/.zshenv ~
rsync -a --delete ./rc/slstatus ~/.config/sckless
rsync -a --delete ./rc/dmenu ~/.config/sckless
rsync -a --delete ./rc/dwm ~/.config/sckless
rsync -a --delete ./scripts ~
