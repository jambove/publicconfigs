#!/bin/sh
rm -rf ./rc ./scripts
mkdir -p ./rc/{zsh,bash,vim,dwm,dmenu,slstatus}
rsync -a --delete --exclude="*errors*" "$XDG_CONFIG_HOME/X11" ./rc
rsync -a --delete "$XDG_CONFIG_HOME/bash/.bashrc" ./rc/bash
rsync -a --delete "$XDG_CONFIG_HOME/zsh/.zshrc" ./rc/zsh
rsync -a --delete "$XDG_CONFIG_HOME/vim/.vimrc" ./rc/vim
rsync -a --delete "$XDG_CONFIG_HOME/env" ./rc
rsync -a --delete "$XDG_CONFIG_HOME/aliasrc" ./rc
rsync -a --delete "$HOME/.zshenv" ./rc
rsync -a --delete "$HOME/sckless/slstatus/config.h" ./rc/slstatus/slstatus
rsync -a --delete "/etc/portage/savedconfig/x11-misc/dmenu-5.0" ./rc/dmenu
rsync -a --delete "/etc/portage/savedconfig/x11-wm/dwm-6.3" ./rc/dwm
rsync -a --delete "$HOME/scripts" .

