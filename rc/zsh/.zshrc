autoload -Uz compinit
compinit
eval "$(dircolors)"
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' menu select

setopt HIST_IGNORE_ALL_DUPS

# Prompt
PROMPT='%F{cyan}%n%f@%F{yellow}%m%f %F{green}%B%~%b%f $ '
RPROMPT='[%F{blue}%?%f]'

# History
HISTFILE="$XDG_CONFIG_HOME"/zsh/.zsh_history
HISTSIZE=50000
SAVEHIST=10000

# Bindkeys
bindkey "${terminfo[kcuu1]}" up-line-or-search
bindkey "${terminfo[kcud1]}" down-line-or-search
bindkey "^[Oc" forward-word
bindkey "^[Od" backward-word
bindkey "^H" backward-kill-word

# Aliases
[ -f "$XDG_CONFIG_HOME/aliasrc" ] && source "$XDG_CONFIG_HOME/aliasrc"

