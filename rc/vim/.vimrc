runtime! gentoo.vim

" This happens after /etc/vim/vimrc(.local) are loaded, so it will override
" any settings in these files.
" If you don't want that to happen, uncomment the below line to prevent
" defaults.vim from being loaded.
" let g:skip_defaults_vim = 1

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set nocompatible

set undodir=$XDG_DATA_HOME/vim/undo
set directory=$XDG_DATA_HOME/vim/swap
set backupdir=$XDG_DATA_HOME/vim/backup
set viewdir=$XDG_DATA_HOME/vim/view
set viminfo+='1000,n$XDG_DATA_HOME/vim/viminfo
set runtimepath=$XDG_CONFIG_HOME/vim,$VIMRUNTIME,$XDG_CONFIG_HOME/vim/after

syntax on

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
"set background=dark

" Uncomment the following to have Vim jump to the last position when
" reopening a file
"au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
filetype plugin indent on

" Colorscheme
colorscheme wombat256grf

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
"set incsearch		" Incremental search
"set autowrite		" Automatically save before commands like :next and :make
"set hidden		" Hide buffers when they are abandoned
set mouse=a		" Enable mouse usage (all modes)
set hlsearch		" Highlight searches <C-L> for temporary off
set wildmenu		" Better command-line completion
set ruler
set nostartofline
set confirm
set number
set relativenumber
set pastetoggle=<F11>
"set visualbell
set nocompatible
filetype plugin on
" apple and trees
"Remaps
vnoremap <C-c> "+y
map <C-p> "+p
nnoremap <C-L> :nohl<CR><C-L>
nnoremap S :%s//gc<Left><Left><Left>
map <leader>o :setlocal spell! spelllang=en<CR>
let g:vimwiki_list = [{'path': '~/suli/Gitlab/jambove.gitlab.io/vimwiki', 'syntax': 'default', 'ext': '.wiki', 'path_html': '~/suli/Gitlab/jambove.gitlab.io/public'}]
autocmd FileType vimwiki let g:vimwiki_text_ignore_newline = 0
call plug#begin('~/config/vim/plugged')
Plug 'vimwiki/vimwiki'
call plug#end()
