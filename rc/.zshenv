#!/bin/zsh

[ -f "$HOME/.config/env" ] && source "$HOME/.config/env"

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
exec ssh-agent startx "$XDG_CONFIG_HOME/X11/xinitrc"
fi
